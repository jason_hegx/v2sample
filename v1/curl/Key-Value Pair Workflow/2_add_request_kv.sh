curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' -d '{
  "requestName": "sample content",
  "sourceNativeId": "introduction page/english",
  "sourceNativeLanguageCode": "en",
  "targetNativeIds": [
    "introduction page/spanish"
  ],
  "targetNativeLanguageCodes": [
    "es"
  ],
  "wordCount": 100,
  "fieldNames": [
    "p1", "p2", "p3"
  ],
  "fieldValues": [
    "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
"Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.",
"In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,"
  ],
  "fieldComments": [
    "p1 field comment",
    "p2 field comment",
    "p3 field comment"
  ]
}' 'https://content-api.lionbridge.com/v1/jobs/e093fbc0-ddbc-4eef-9469-aad71f23ab38/requests/add'


# [
  # {
    # "requestId": "e8e7dc48-678a-4eec-80a4-5955f222a82e",
    # "jobId": "e093fbc0-ddbc-4eef-9469-aad71f23ab38",
    # "requestName": "sample content",
    # "statusCode": {
      # "statusCode": "PENDING"
    # },
    # "hasError": false,
    # "sourceNativeId": "introduction page/english",
    # "sourceNativeLanguageCode": "en",
    # "targetNativeId": "introduction page/spanish",
    # "targetNativeLanguageCode": "es",
    # "createdDate": "2017-12-19T14:29:43.740Z",
    # "modifiedDate": "2017-12-19T14:29:43.740Z",
    # "wordCount": 100
  # }
# ]