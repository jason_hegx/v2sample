# create a new job

curl --location --request POST 'https://content-api.staging.lionbridge.com/v2/jobs' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \
--data-raw '{
    "jobName": "Test Job-SK",
    "description": "New test Job for testing",
    "poReference": "9692",
    "dueDate": "2020-01-01",
    "providerId": "%%%PROVIDER_ID_STRING%%%"
   
}'

#Response

{
    "jobId": "95CklFg5jZ",
    "jobName": "Test Job-SK",
    "description": "New test Job for testing",
    "statusCode": "CREATED",
    "hasError": false,
    "creatorId": "kche#MOCK",
    "providerId": "%%%PROVIDER_ID_STRING%%%",
    "poReference": "9692",
    "dueDate": "2020-01-01T00:00:00Z",
    "createdDate": "2021-01-12T07:10:45.534Z",
    "modifiedDate": "2021-01-12T07:10:45.534Z",
    "archived": false,
    "shouldQuote": false,
    "siteId": "%%%SITE_ID_STRING%%%"
}