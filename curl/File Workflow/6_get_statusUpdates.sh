#Get all unacknowledged status updates

curl --location --request GET 'https://content-api.staging.lionbridge.com/v2/statusupdates' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \

#Response

{
    "_embedded": {
        "statusupdates": [
            {
                "updateId": "BwHZOxXUSxm44-b--nK8aw",
                "jobId": "WfF4sGC9jd",
                "requestIds": [
                    "S4SZrznPRZGfjCQ0aUd_nw"
                ],
                "statusCode": "REVIEW_TRANSLATION",
                "updateTime": "2021-03-02T07:39:13.477Z",
                "hasError": false
            },
            {
                "updateId": "owMFi7NHTCqeHOfq4Cxv2g",
                "jobId": "WfF4sGC9jd",
                "requestIds": [
                    "S4SZrznPRZGfjCQ0aUd_nw"
                ],
                "statusCode": "IN_TRANSLATION",
                "updateTime": "2021-03-02T07:35:03.613Z",
                "hasError": false
            }
        ]
    }
}