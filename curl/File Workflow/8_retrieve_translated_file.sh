#Retrieve the target content for translation request as a file

curl --location --request GET 'https://content-api.staging.lionbridge.com/v2/jobs/WfF4sGC9jd/requests/S4SZrznPRZGfjCQ0aUd_nw/retrievefile' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \

#Response

<?xml version="1.0" encoding="utf-8"?>
<ToTranslateXml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Id>Wsnjr5h7jM</Id>
  <ItemToTranslate Id="04CCE9A168AF482EAECD4BAD037FEB5F">
    <Name>Créé à l'heure locale : mardi 9 février, 2021 11:22:18 AM. Nom de l'élément à traduire [1]</Name>
    <Description>Créé à l'heure locale : mardi 9 février, 2021 11:22:18 AM. Une description</Description>
    <Text>Créé à l'heure locale : mardi 9 février, 2021 11:22:18 AM. texte à traduire</Text>
  </ItemToTranslate>
</ToTranslateXml>