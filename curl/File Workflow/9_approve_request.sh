# Approve translated content of specified request(s)

curl --location --request PUT 'https://content-api.staging.lionbridge.com/v2/jobs/WfF4sGC9jd/requests/approve' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \
--data-raw '{
    "requestIds": [
        "S4SZrznPRZGfjCQ0aUd_nw"
    ]
}'

#Response

{
    "_embedded": {
        "requests": [
            {
                "requestId": "S4SZrznPRZGfjCQ0aUd_nw",
                "jobId": "WfF4sGC9jd",
                "requestName": "test request",
                "statusCode": "TRANSLATION_APPROVED",
                "hasError": false,
                "sourceNativeId": "en",
                "sourceNativeLanguageCode": "en-US",
                "targetNativeId": "fr",
                "targetNativeLanguageCode": "fr-FR",
                "createdDate": "2021-03-02T07:32:58.123Z",
                "modifiedDate": "2021-03-02T08:16:45.069Z",
                "fileId": "b29e0fb0-237a-4976-a7bf-f5f89b7e5cf4",
                "fileType": "xml",
                "providerReference": "BV-000377758"
            }
        ]
    }
}

